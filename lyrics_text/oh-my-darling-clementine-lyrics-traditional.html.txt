Oh My Darling Clementine
Near a cavern, across from a canyon,
Excavating for a mine,
Lived a miner, forty-niner
And his daughter ClementineOh my Darling, Oh my Darling,
Oh my Darling Clementine.
You are lost and gone forever,
Dreadful sorry, Clementine.Light she was and like a fairy,
And her shoes were number nine
Herring boxes without topses
Sandals were for Clementine.CHORUS:Drove she ducklings to the water
Every morning just at nine,
Hit her foot against a splinter
Fell into the foaming brine.CHORUS:Ruby lips above the water,
Blowing bubbles soft and fine,
But alas, I was no swimmer,
So I lost my Clementine.CHORUS:CHORUS:Then the miner, forty-niner,
Soon began to peak and pine,
Thought he oughter join his daughter,
Now he's with his Clementine.CHORUS:
In the church yard in the canyon
Where the myrtle doth entwine
There grows roses and other posies
Fertilized by Clementine.CHORUS:
How i missed her, how i missed her, how i missed my Clementine!
Til' i kissed her little sister, and forgot my Clementine.