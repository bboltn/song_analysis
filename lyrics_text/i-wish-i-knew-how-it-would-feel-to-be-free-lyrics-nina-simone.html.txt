I Wish I Knew How It Would Feel to Be Free
I wish I knew how
It would feel to be free
I wish I could break
All the chains holdin' me
I wish I could say
All the things that I should saySay 'em loud, say 'em clear
For the whole 'round world to hear
I wish I could share
All the love that's in my heart
Remove all the thoughts
That keep us apartI wish you could know
What it means to be me
Then you'd see and agree
That every man should be freeI wish I could give
All I'm longin' to give
I wish I could live
Like I'm longing to live
I wish I could do
All the things that I can doAnd though I'm way over due
I'd be startin' a new
Well I wish I could be
Like a bird in the sky
How sweet it would be
If I found I could flyI'd soar up to the sun
And look down at the sea
And I'd sing 'cause I know yeahAnd I'd sing 'cause I know yeah
And I'd sing 'cause I know
I'd know how it feels
I'd know how it feels to be free, yeah-yeah
I-I'd know how it feels
Yes, I'd know
I'd know how it feels, how it feels
To be free, no no no