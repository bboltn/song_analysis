Fall In Line
[Verse 1]
Little girls
Listen closely
Cause no one told me
But you deserve to know
That in this world, you are not beholden
You do not owe them
Your body and your soul[Pre-Chorus]
All the youth in the world will not save you from growing older
And all the truth in the girl is too precious to be stolen from her[Chorus]
It's just the way it is
Maybe it's never gonna change
But I got a mind to show my strength
And I got my right to speak my mind
And I'm gonna pay for this
They're gonna burn me at the stake
But I got a fire in my veins
I wasn't made to fall in line
No, I wasn't made to fall in line
No[Verse 2]
Show some skin
Make him want you
Cause God forbid you
Know your own way home
And ask yourself why it matters
Who it flatters
You're more than flesh and bones[Pre-Chorus]
All the youth in the world will not save you from growing older
And all the truth in the girl is too precious to be stolen from her[Chorus]
It's just the way it is
Maybe it's never gonna change
But I got a mind to show my strength
And I got my right to speak my mind
And I'm gonna pay for this
They're gonna burn me at the stake
But I got a fire in my veins
I wasn't made to fall in line
No, I wasn't made to fall in line
No[Bridge: Distorted Male Voice]
1 2 3
Right 2 3
Shut your mouth
Stick your ass out for me
March 2 3
1 2 3
Who told you
You're allowed to think[Chorus]
It's just the way it is
Maybe it's never gonna change
But I got a mind to show my strength
And I got my right to speak my mind
And I'm gonna pay for this
They're gonna burn me at the stake
But I got a fire in my veins
I wasn't made to fall in line
No, I wasn't made to fall in line
No[Outro]
1 2 3
Right 2 3
Shut your mouth
Stick your ass out for me
March 2 3
1 2 3
Who told you
You're allowed to think