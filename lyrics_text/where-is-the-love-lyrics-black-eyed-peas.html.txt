Where Is The Love?
What's wrong with the world, mama
People livin' like they ain't got no mamas
I think the whole world addicted to the drama
Only attracted to things that'll bring you traumaOverseas, yeah, we try to stop terrorism
But we still got terrorists here livin'
In the USA, the big CIA
The Bloods and The Crips and the KKKBut if you only have love for your own race
Then you only leave space to discriminate
And to discriminate only generates hate
And when you hate then you're bound to get irate, yeahMadness is what you demonstrate
And that's exactly how anger works and operates
Man, you gotta have love just to set it straight
Take control of your mind and meditate
Let your soul gravitate to the love, y'all, y'allPeople killin', people dyin'
Children hurt and you hear them cryin'
Can you practice what you preach?
Or would you turn the other cheek?Father, Father, Father help us
Send some guidance from above
'Cause people got me, got me questionin'
Where is the love (Love)Where is the love (The love)
Where is the love (The love)
Where is the love, the love, the loveIt just ain't the same, old ways have changed
New days are strange, is the world insane?
If love and peace are so strong
Why are there pieces of love that don't belong?Nations droppin' bombs
Chemical gasses fillin' lungs of little ones
With ongoin' sufferin' as the youth die young
So ask yourself is the lovin' really goneSo I could ask myself really what is goin' wrong
In this world that we livin' in people keep on givin' in
Makin' wrong decisions, only visions of them dividends
Not respectin' each other, deny thy brother
A war is goin' on but the reason's undercoverThe truth is kept secret, it's swept under the rug
If you never know truth then you never know love
Where's the love, y'all, come on (I don't know)
Where's the truth, y'all, come on (I don't know)
Where's the love, y'allPeople killin', people dyin'
Children hurt and you hear them cryin'
Can you practice what you preach?
Or would you turn the other cheek?Father, Father, Father help us
Send some guidance from above
'Cause people got me, got me questionin'
Where is the love (Love)Where is the love (The love)?
Where is the love (The love)?
Where is the love (The love)?
Where is the love, the love, the love?I feel the weight of the world on my shoulder
As I'm gettin' older, y'all, people gets colder
Most of us only care about money makin'
Selfishness got us followin' the wrong directionWrong information always shown by the media
Negative images is the main criteria
Infecting the young minds faster than bacteria
Kids wanna act like what they see in the cinemaYo', whatever happened to the values of humanity
Whatever happened to the fairness and equality
Instead of spreading love we're spreading animosity
Lack of understanding, leading us away from unityThat's the reason why sometimes I'm feelin' under
That's the reason why sometimes I'm feelin' down
There's no wonder why sometimes I'm feelin' under
Gotta keep my faith alive 'til love is found
Now ask yourselfWhere is the love?
Where is the love?
Where is the love?
Where is the love?Father, Father, Father, help us
Send some guidance from above
'Cause people got me, got me questionin'
Where is the love?Sing with me y'all:
One world, one world (We only got)
One world, one world (That's all we got)
One world, one world
And something's wrong with it (Yeah)
Something's wrong with it (Yeah)
Something's wrong with the wo-wo-world, yeah
We only got
(One world, one world)
That's all we got
(One world, one world)