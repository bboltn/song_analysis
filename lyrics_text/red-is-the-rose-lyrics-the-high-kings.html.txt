Red Is The Rose
Come over the hills, my bonnie Irish lass
Come over the hills to your darling
You choose the road, love, and I'll make the vow
And I'll be your true love forever.Chorus
Red is the rose that in yonder garden grows
Fair is the lily of the valley
Clear is the water that flows from the Boyne
But my love is fairer than any.'Twas down by Killarney's green woods that we strayed
When the moon and the stars they were shining
The moon shone its rays on her locks of golden hair
And she swore she'd be my love forever.ChorusIt's not for the parting that my sister pains
It's not for the grief of my mother
'Tis all for the loss of my bonny Irish lass
That my heart is breaking forever.Chorus (x2)