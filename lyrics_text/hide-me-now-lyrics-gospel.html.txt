Hide Me Now
Hide me now
Under Your wings
Cover me
Within Your mighty handWhen the oceans rise and thunders roar
I will soar with you above the storm
Father, You are King over the flood
I will be still and know You are GodFind rest my soul
In Christ alone
Know His power
In quietness and trustWhen the oceans rise and thunders roar
I will soar with you above the storm
Father, You are King over the flood
I will be still and know You are God
Your Joy Oh lord
will be my strength,
Renouncing fear
We stand in your glorious grace.
When the oceans rise and thunders roar
I will soar with you above the storm
Father, You are King over the flood
I will be still and know You are God