The Skye Boat Song
Speed bonnie boat like a bird on the wing
Onward the sailors cry.
Carry the lad that's born to be king
Over the sea to SkyeLoud the wind howls
loud the waves roarThunderclaps rend the air
Baffled our foes
stand by the shore
Follow they will not dareSpeed bonnie boat like a bird on the wing
Onward the sailors cry.
Carry the lad that's born to be king
Over the sea to SkyeMany's the lad fought on that day
Well the claymore did wield
When the night came
silently lain
Dead on Colloden fieldSpeed bonnie boat like a bird on the wing
Onward the sailors cry.
Carry the lad that's born to be king
Over the sea to SkyeThough the waves heave
soft will ye sleep
Ocean's a royal bed
Rocked in the deep
Flora will keep
Watch by your weary headSpeed bonnie boat like a bird on the wing
Onward the sailors cry.
Carry the lad that's born to be king
Over the sea to Skye