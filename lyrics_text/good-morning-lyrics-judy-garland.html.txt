Good Morning
Good morning, good morning
We've danced the whole night through
Good morning, good morning to youGood morning, good morning
It's great to stay up late
Good morning, good morning to youWhen the band began to play
The sun was shining bright
Now the milkman's on his way
It's too late to say goodnightSo, good morning, good morning
Sunbeams will soon smile through
Good morning, my darling, to youHere we are together
A couple of stand-uppers
Our day is done, breakfast time
Starts with our supperHere we are together
Ah, but the best of friends must party
So let me sing this party song
From the bottom of my heartyGood morning, it's a lovely morning
Good morning, what a wonderful day
We danced the whole night through
Good morning, good morning to youI said, "Good morning, see the sun is shining"
Good morning, hear the birdies sing
It's great to stay up late
Good morning, good morning to youWhen the band began to play
The stars were shining bright
Now the milkman's on his way
It's too late to say goodnightGood morning, good morning
Sunbeams will soon smile through
Good morning, good morning
Good morning, my darling, to you