Break The Distance
If I had a chance
I'd take it faster than a heartbeat
If I caught a glance,
I'd run to you like nobody was watching
But it's so hard to get to you
Cause when you see the sun, I see the moonBuckets of water, millions of clouds
Miles of road just to get where you are
Oh I wish, it wasn't so far...
Thousands of dollars just to be near
Countless thoughts of you being here
Oh I wish, it wasn't so hard
But maybe one day we can-
Break the DistanceI know it's hard,
For everyone to understand it
But you're in my heart,
Even though it feels like we got stranded
And I wish I could get to you
See you face to face
Without a screen cutting throughBuckets of water, millions of clouds
Miles of road just to get where you are
Oh I wish, it wasn't so far...
Thousands of dollars just to be near
Countless thoughts of you being here
Oh I wish, it wasn't so hard
But maybe one day we can-
Break the DistanceClouds will you please help me out
I need to go and see them now
Pick me up, off my feet
I know that it's crazy
But I am ready with my suitcase by meClouds will you please help me out
I need to go and see them now
Pick me up, off my feet
I know that it's crazy
But I am ready with my suitcase by meBuckets of water, millions of clouds
Miles of road just to get where you are
Oh I wish, it wasn't so far...
Thousands of dollars just to be near
Countless thoughts of you being here
Oh I wish, it wasn't so hard
But maybe one day we can-
Break the Distance