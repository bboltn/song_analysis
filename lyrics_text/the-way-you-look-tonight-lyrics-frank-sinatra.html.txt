The Way You Look Tonight
Some day, when I'm awfully low
When the world is cold
I will feel a glow just thinking of you
And the way you look tonightYes, you're lovely, with your smile so warm
And your cheeks so soft
There is nothing for me but to love you
And the way you look tonightWith each word your tenderness grows
Tearin' my fear apart
And that laugh.wrinkles your nose
Touches my foolish heartLovely ... Never, never change
Keep that breathless charm
Won't you please arrange it? 'cause I love you
Just the way you look tonightAnd that laugh that wrinkles your nose
It touches my foolish heartLovely ... Don't you ever change
Keep that breathless charm
Won't you please arrange it? 'cause I love you
a-just the way you look tonightMm, Mm Mm, Mm,
Just the way you look tonight