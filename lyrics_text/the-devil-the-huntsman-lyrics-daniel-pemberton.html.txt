The Devil & The Huntsman
Young man came from hunting faint, tired and weary
What does ail my lord, my dearie?
Oh, brother dear, let my bed be made
For I feel the gripe of the woody nightshadeMen need a man would die as soon
Out of the light of a mage's moon
'Twas not by bolt, but yet by blade
Can break the magic that the devil made'Twas not by fire, but was forged in flame
That can drown the sorrows of a huntsman's painThis young man he died fair soon
By the light of the hunters' moon
'Twas not by bolt, nor yet by blade
of the berries of the woody nightshadeOh father dear lie here be safe (?)
From the path that the devil made"