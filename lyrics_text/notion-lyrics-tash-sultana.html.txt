Notion
I hear the birds
When they're singing
I hear the sirens
When they're ringing
But I can't take my mind off of you
A thousand words, pouring out
The distance of over there
Is moving on and out
You said it'd be alright
A thousand words, pouring out
The distance of over there
Is moving on and out
You said it'd be alrightHow 'bout these notions
Hmm, they're deep as ocean
Calling out my name
Screaming out in vain
Singing hallelujahTell your mother, she don't understand
Tell your mother, she's not listening
Why don't you tear my heart
It chains me to [?]
Have all the dice. [?]
That don't matter, [?]
I will follow you
Into the darkHow 'bout these notions
Hmm, they're deep as ocean
Calling out my name
Screaming out in vain
Singing hallelujahThese notions
Hmm, they're deep as ocean
Calling out my name
Screaming out in vain
Singing hallelujahSinging hallelujah to you
Calling out my name
I'm in the deep end, ocean
Calling out my name
Singing hallelujah to you