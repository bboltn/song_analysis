Sway
When marimba rhythms start to playDance with me, make me sway
Like a lazy ocean hugs the shore
Hold me close, sway me more
Like a flower bending in the breezeBend with me, sway with ease
When we dance you have a way with me
Stay with me, sway with me
Other dancers may be on the floorDear, but my eyes will see only you
Only you have that magic technique
When we sway I go weak
I can hear the sounds of violinsLong before it begins
Make me thrill as only you know how
Sway me smooth, sway me now
Other dancers may be on the floorDear, but my eyes will see only you
Only you have that magic technique
When we sway I go weak
I can hear the sounds of violinsLong before it begins
Make me thrill as only you know how
Sway me smooth, sway me now
When marimba rhythms start to playDance with me, make me sway
Like a lazy ocean hugs the shore
Hold me close, sway me more
Like a flower bending in the breezeBend with me, sway with ease
When we dance you have a way with me
Stay with me, sway with me
When marimba start to play
Hold me close, make sway
Like a ocean hugs the shore
Hold me close, sway me more
Like a flower bending in the breeze
Bend with me, sway with ease
When we dance you have a way with me
Stay with me, sway with me