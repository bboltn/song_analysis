Your Presence Is Heaven
Who is like You Lord in all the earth?
Matchless love and beauty, endless worth
Nothing in this world can satisfy
'Cause Jesus You're the cup that won't run dryYour presence is heaven to me Your presence is heaven to meTreasure of my heart and of my soul
In my weakness you are merciful
Redeemer of my past and present wrongs
Holder of my future days to comeYour presence is heaven to me Your presence is heaven to meAll my days on earth I will await
The moment that I see You face to face
Nothing in this world can satisfy
'Cause Jesus You're the cup that won't run dry
Your presence is heaven to me Your presence is heaven to me
Oh Jesus, Oh Jesus Your presence is heaven to me
Oh Jesus, Oh Jesus Your presence is heaven to me