Hold On
I think you were just what our Colin needed.
What you've got to do is finish
What you have begun!
I don't know just how
But it's not over till you've won.When you see the storm is coming
See the lightning part the skies
It's too late to run
There's terror in your eyes
What you do then is remember
This old thing you heard me say
"It's the storm, not you
That's bound to blow away"Hold on
Hold on to someone standing by
Hold on
Don't even ask how long or why?
Child, hold on to what you know is true
Hold on till you get through
Child, oh child
Hold onWhen you feel your heart is poundin'
Fear a devil's at your door
There's no place to hide
You're frozen to the floor
What you do then is you force yourself
To wake up, and just say
"It's this dream, not me
That's bound to go away"Hold on
Hold on, the night will soon be by
Hold on
Until there's nothing left to try
Child, hold on, there's angels on their way
Hold on and hear them say
"Child, oh child!"And it doesn't even matter
If the danger and the doom
Come from up above or down below
Or just come flying at you from across the roomWhen you see a man who's raging
And he's jealous and he fears
That you've walked through walls
He's hid behind for years
What you do then
Is you tell yourself to wait it out
And say "It's this day, not me
That's bound to go away"Child, oh hold on
It's this day, not you
That's bound to go away