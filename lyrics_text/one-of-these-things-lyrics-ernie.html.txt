One of These Things
Sesame Street
Miscellaneous
One Of These Things (Is Not Like The Others)
One of these things is not like the others,
One of these things just doesn't belong,
Can you tell which thing is not like the others
By the time I finish my song?Did you guess which thing was not like the others?
Did you guess which thing just doesn't belong?
If you guessed this one is not like the others,
Then you're absolutely... right!Another version:Three of these things belong together
Three of these things are kind of the same
Can you guess which one of these doesn't belong here?
Now it's time to play our game (time to play our game).Bonus VersionThree of these kids belong together
Three of these kids are kind of the same
But one of these kids is doing his (her) own thing
Now it's time to play our game
It's time to play our game.