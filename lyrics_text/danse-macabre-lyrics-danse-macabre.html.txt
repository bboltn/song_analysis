Danse Macabre
Death is tapping its foot
To a bewitching tune
Truly in a joyful mood
Clouds covering the moonIn the midnight hour
The dead perform a danceDanse Macabre Danse Macabre Danse Macabre
In the midnight hour
The dead perform a dancePeasants, nobles or kings
Like puppets on a string
Dance they all will
When Death spreads its wingsIn the midnight hour
The dead perform a danceDanse Macabre Danse Macabre Danse Macabre
In the midnight hour
The dead perform a danceDanse Macabre Danse Macabre Danse Macabre
Like withered flowers
Now skeletons in tranceOne can hear the North winds blow
But louder is the violin
The living, their heads they bow
For they know Death will always winDanse Macabre Danse Macabre Danse Macabre
In the midnight hour
The dead perform a danceDanse Macabre Danse Macabre Danse Macabre
Like withered flowers
Now skeletons in tranceDanse Macabre Danse Macabre Danse Macabre
It's already certain
For Death allows no chance