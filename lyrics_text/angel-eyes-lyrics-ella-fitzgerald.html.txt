Angel Eyes
Try to think that love's not around
But it's uncomfortably near
My old heart ain't gaining no ground
Because my angel eyes ain't hereAngel eyes, that old Devil sent
They glow unbearably bright
Need I say that my love's misspent
Misspent with angel eyes tonightSo drink up all you people
Order anything you see
Have fun you happy people
The laughs and the jokes on mePardon me but I got to run
The fact's uncommonly clear
Got to find who's now number one
And why my angel eyes ain't here
Oh, where is my angel eyesExcuse me while I disappear
Angel eyes, angel eyes