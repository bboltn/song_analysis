Try
I don't feel the way I used to
The sky is grey much more than it is blue
But I know one day I'll get through
And I'll take my place again
So I will try
So I will tryI don't love the way I need to
You need more and I know that much is true
So I'll fight for our breakthrough
And I'll breathe in you again
If I would try
If I would tryThere is no one for me to blame
cause I know the only thing in my way
Is meI don't live the way I want to
That whole picture never came into view
And I'm tired of getting used to
The day
So I will try
So I will try
Yep, I will try
If I would try