Judgement Day
I can feel the floor shaking, and the glass begin to break
The air is getting thinner with every breath that I take
The calm before the storm, you can hear the drop of a pin
Never been claustrophobic, but now the walls are closing inI've crossed every line, broken every boundary
And now it's retribution time 'cause the change that I'm into
It ain't that holy!So, strike me down, take me away
Tax are due, it's time to pay
Face what I deserve, here comes JUDGEMENT DAY!
I won't run, the guilt is mine
Still I'm denying all my crimes
Face what I deserve, here comes JUDGEMENT DAY!Of all the love I have taken
All the hearts I've turned to hate
Hearts are easily broken when you're being made in the shade
Crossed every line, broken every boundary
And now it's retribution time 'cause the change that I'm into
It ain't that holy!So, strike me down, take me away
Tax are due, it's time to pay
Face what I deserve, here comes JUDGEMENT DAY!
I won't run, the guilt is mine
Still I'm denying all my crimes
Face what I deserve, here comes JUDGEMENT DAY!Standing at the gate
Ready to meet my faith
Burns my soul it's not too late
So bring on JUDGEMENT DAY!So, strike me down, take me away
Tax are due, it's time to pay
Face what I deserve, here comes JUDGEMENT DAY!
I won't run, the guilt is mine
Still I'm denying all my crimes
Face what I deserve, here comes JUDGEMENT DAY!