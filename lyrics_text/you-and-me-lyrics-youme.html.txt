You and Me
You and me were always with each other
Before we knew the other was ever there
You and me we belong together
Just like a breath needs the airI told you if you called I would come runnin'
Across the highs the lows and the in between
You and me, we've got two minds that think as one
And our hearts march to the same beat
They say everything it happens for a reason
You can be flawed enough but perfect for a person
Someone who will be there for you when you fall apart
Guiding your direction when you're riding through the dark
Oh, that's you and meYou and me, we're searching' for the same light
Desperate for a cure to this disease
Well, some days are better than others
But I fear no thing as long as you're with me
They say everything' it happens for a reason
You can be flawed enough but perfect for a person
Someone who will be there for you when you fall apart
Guiding your direction when you're riding through the darkAnd they say everything it happens for a reason
You can be flawed enough but perfect for a person
Someone who will be there for you when you start to fall apart
Guiding your direction when you're riding through the darkOh, that's you and me
Oh, that's you and me
Oh, that's you and me
That's you and me