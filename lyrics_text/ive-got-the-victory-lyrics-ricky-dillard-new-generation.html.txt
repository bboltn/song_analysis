I've Got the Victory
Verse
The devil thought he had me
But I got away
All because I (I've got the victory)
Sickness had me bound
But now I'm free
All because I (I've got the victory)
The devil thought that he had me
But Jesus came and grabbed me
(There's no doubt in my mind, I, I've got the victory)Bridge
(I've come through many trial)
(I've come through sickness and pain)
They said I wouldn't make it
They said I wouldn't be here today
But by his grace, and by his love
I've already overcomeVamp 1
I've got the victory
Yes, I've got itVamp 2
I've got the victoryVerse
you got it (I got it)
Let me see you wave your hand (I got it)
And if you got it (I got it)
Let me see your leap for joyVamp 3
I got itVerse
If you got
Let me see your wave your hand
When the devil said no
God said yes