Sleep On The Floor
Pack yourself a toothbrush, dear
Pack yourself a favorite blouse
Take a withdrawal slip
Take all of your savings out
'Cause if we don't leave this town
We might never make it out
I was not born to drown
Baby come onForget what Father Brennan said
We were not born in sin
Leave a note on your bed
Let your mother know you're safe
And by the time she wakes
We'll have driven through the state
We'll have driven through the nigh
Baby come onIf the sun don't shine on me today
And if the subways flood and bridges break
Will you lay yourself down and dig your grave
Or will you rail against your dying dayAnd when we looked outside, couldn't even see the sky
How do you pay the rent, is it your parents
Or is hard work dear, holding the atmosphere
I don't wanna live like thatIf the sun don't shine on me today
If the subways flood and bridges breakJesus Christ can't save me tonight
Put on your dress, yes wear something nice
Decide on me, yea decide on us
Oh, oh, oh, Illinois, IllinoisPack yourself a toothbrush, dear
Pack yourself a favorite blouse
Take a withdrawal slip
Take all of your savings out
Cause if we don't leave this town
We might never make it out