Back Home Again In Indiana
Back home again in Indiana,
And it seems that I can see
The gleaming candle light, still burning bright,
Through the Sycamores for me.
The new-mown hay sends all its fragnance
Through the fields I used to roam.
When I dream about the moonlight on the Wabash
Then I long for my Indiana home.
[ piano ]
(When I dream about the moonlight on the Wabash
Then I long for my Indiana home sweet home)Back home again in Indiana...
(Indiana Indiana Indiana home)