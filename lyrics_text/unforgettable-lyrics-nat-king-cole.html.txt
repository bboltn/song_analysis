Unforgettable
Unforgettable, that's what you are
Unforgettable though near or far
Like a song of love that clings to me
How the thought of you does things to me
Never before has someone been moreUnforgettable in every way
And forever more, that's how you'll stay
That's why, darling, it's incredible
That someone so Unforgettable
Thinks that I am Unforgettable tooUnforgettable in every way
And forever more, that's how you'll stay
That's why, darling, it's incredible
That someone so Unforgettable
Thinks that I am Unforgettable too