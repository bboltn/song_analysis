Bei Mir Bist Du Schoen
Of all the boys I've known, and I've known some
Until I first met you, I was lonesome
And when you came in sight, dear, my heart grew light
And this old world seemed new to meYou're really swell, I have to admit you
Deserve expressions that really fit you
And so I've racked my brain, hoping to explain
All the things that you do to meBei mir bist du schoen, please let me explain
Bei mir bist du schoen means you're grand
Bei mir bist du schoen, again I'll explain
It means you're the fairest in the landI could say "Bella, bella", even say "Wunderbar"
Each language only helps me tell you how grand you areI've tried to explain, bei mir bist du schoen
So kiss me and say you understandBei mir bist du schoen, you've heard it all before
but let me try to explain
Bei mir bist du schoen means that you're grand
Bei mir bist du schoen, it's such an old refrain
and yet I should explain
It means I am begging for your handI could say "Bella, bella", even say "Wunderbar"
Each language only helps me tell you how grand you areI could say "Bella, bella", even say "Wunderbar"
Each language only helps me tell you how grand you areI've tried to explain, bei mir bist du schoen
So kiss me and say that you will understand