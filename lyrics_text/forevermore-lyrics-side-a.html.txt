Forevermore
there are times
when i just want to look at your face
with the stars in the night
there are times
when i just want to feel your embrace
in the cold of the night
i just cant believe that you are mine now
you were just a dream that i once knew
i never thought i would be right for youi just cant compare you with
anything in this world
you're all i need to be with forevermore
all those years ive longed to hold you in my arms
i've been dreaming of you
every night
i've been watching all the stars that fall downwishing you would be mine
i just cant believe that you were mine now
time and again
there are these changes that we cannot end
sure a star that keeps going on and on
my love for you will be forevermorewishing you would be minei just cant believe that you were mine now
you were just a dream that i once knew
i never thought i would be right for you
i just cant compare you with
anything in this world
as endless as forever
our love will stay togetheryou're all i need to be with forever more
(as endless as forever
our love will stay together)
you're all i need
to be with forevermore...