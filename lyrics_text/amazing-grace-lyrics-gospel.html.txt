Amazing Grace
Amazing Grace, how sweet the sound,
That saved a wretch like me.
I once was lost but now I'm found,
Was blind, but now I see.'twas Grace that taught,
my heart to fear.
And grace, my fears relieved.
How precious did that grace appear,
the hour I first believed.Through many dangers, toils and snares,
I have already come. 'tis grace that brought me safe thus far,
and grace will lead us home.The Lord has promised good to me,
His word my hope secures.
He will my shield and portion be,
as long as life endures.When we've been there ten thousand years,
bright shining as the sun.
We've no less days to sing God's praise,
than when we first begun.Amazing Grace, how sweet the sound,
That saved a wretch like me.
I once was lost but now am found,
Was blind, but now... I see