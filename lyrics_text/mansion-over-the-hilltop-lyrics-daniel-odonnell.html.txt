Mansion Over the Hilltop
I'm satisfied with just a cottage below
A little silver and a little gold
But in that city where the ransomed will shine
I want a gold one that's silver linedI've got a mansion just over the hilltop
In that bright land where we'll never grow old
And some day yonder we'll never more wander
But walk on streets that are purest goldTho' often tempted, tormented and tested
And, like the prophet, my pillow a stone
And tho' I find here no permanent dwelling
I know He'll give me a mansion my ownDon't think me poor or deserted or lonely
I'm not discouraged I'm heaven bound
I'm just a pilgrim in search of the city
I want a mansion, a harp and a crown