I Can't Make You Love Me
Turn down the lights, turn down the bed
Turn down these voices inside my head
Lay down with me, tell me no lies
Just hold me close, don't patronize
Don't patronize me'Cause I can't make you love me if you don't
You can't make your heart feel somethin' it won't
Here in the dark, in these final hours
I will lay down my heart and I'll feel the power
But you won't, no you won't
'Cause I can't make you love me, if you don'tI'll close my eyes, then I won't see
The love you don't feel when you're holdin' me
Mornin' will come and I'll do what's right
Just give me till then to give up this fight
And I will give up this fight'Cause I can't make you love me if you don't
You can't make your heart feel somethin' it won't
Here in the dark, in these final hours
I will lay down my heart and I'll feel the power
But you won't, no you won't
'Cause I can't make you love me, if you don't