Silence
Give me release
Witness me
I am outside
Give me peaceHeaven holds a sense of wonder
And I wanted to believe
That I'd get caught up
When the rage in me subsidesIn this white wave
I am sinking
In this silence
In this white wave
In this silence
I believeI can't help this longing
Comfort me
I can't hold it all in
If you won't let meHeaven holds a sense of wonder
And I wanted to believe
That I'd get caught up
When the rage in me subsidesIn this white wave
I am sinking
In this silence
In this white wave
In this silence
I believeI have seen you
In this white wave
You are silent
You are breathing
In this white wave
I am free