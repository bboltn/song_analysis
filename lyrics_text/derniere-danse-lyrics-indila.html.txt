Derniere Danse
Oh ma douce souffrance
Pourquoi s'acharner? Tu recommences
Je ne suis qu'un être sans importance
Sans lui, je suis un peu paro
Je déambule seule dans le metro
Une dernière danse
Pour oublier ma peine immense
Je veux m'enfuir que tout recommence
Oh ma douce souffranceJe remue le ciel, le jour, la nuit
Je danse avec le vent, la pluie
Un peu d'amour, un brin de miel
Et je danse, danse, danse, danse, danse, danse, danse
Et dans le bruit, je cours et j'ai peur
Est-ce mon tour?
Vient la douleur...
Dans tout Paris, je m'abandonne
Et je m'envole, vole, vole, vole, vole, vole, voleQue d'espérance
Sur ce chemin en ton absence
J'ai beau trimer, sans toi ma vie n'est qu'un décor qui brille, vide de sensJe remue le ciel, le jour, la nuit
Je danse avec le vent, la pluie
Un peu d'amour, un brin de miel
Et je danse, danse, danse, danse, danse, danse, danse
Et dans le bruit, je cours et j'ai peur
Est-ce mon tour?
Vient la douleur...
Dans tout Paris, je m'abandonne
Et je m'envole, vole, vole, vole, vole, vole voleDans cette douce souffrance
Dont j'ai payé toutes les offenses
Ecoute comme mon cœur est immense
Je suis une enfant du mondeJe remue le ciel, le jour, la nuit
Je danse avec le vent, la pluie
Un peu d'amour, un brin de miel
Et je danse, danse, danse, danse, danse, danse, danse
Et dans le bruit, je cours et j'ai peur
Est-ce mon tour?
Vient la douleur...
Dans tout Paris, je m'abandonne
Et je m'envole, vole, vole, vole, vole, vole vole