A Mothers Love
Thank you for watching over me
All of the sleepless nights you lay awake
Thank you for knowing when to hold me close
and when to let me goThank you for every stepping stone
And for the path that always leads me home
I thank you for the time you took
to see the heart inside of meYou gave me the roots to start this life
and then you gave me wings to fly
And I learned to dream
Because you believed in meThere's no power like it on this earth
No treasure equal to its worth
The gift of a mother's loveThank you for every sunlit day
That filled the corners of my memory
Thank you for every selfless unsung deed
I know you did for meThank you for giving me the choice
To search my soul till I could find my voice
And I thank you for teaching me
To be strong enough to bendYou gave me the roots to start this life
and then you gave me wings to fly
And I learned to dream
Because you believed in meThere's no power like it on this earth
No treasure equal to its worth
The gift of a mother's loveI thank God for a mother's love