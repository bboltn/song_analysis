Whisky In The Jar
As I was a goin' over the far famed Kerry mountains
I met with captain Farrell and his money he was counting
I first produced my pistol and I then produced my rapier
Saying "Stand and deliver" for he were a bold deceiverMush-a ring dum-a do dum-a da
Wack fall the daddy-o, wack fall the daddy-o
There's whiskey in the jarI counted out his money and it made a pretty penny
I put it in me pocket and I took it home to Jenny
She sighed and she swore that she never would deceive me
But the devil take the women for they never can be easy()I went up to my chamber, all for to take a slumber
I dreamt of gold and jewels and for sure 't was no wonder
But Jenny blew me charges and she filled them up with water
Then sent for captain Farrell to be ready for the slaughter()'t was early in the morning, just before I rose to travel
Up comes a band of footmen and likewise captain Farrell
I first produced me pistol for she stole away me rapier
I couldn't shoot the water, so a prisoner I was taken()If anyone can aid me 't is my brother in the army
If I can find his station in Cork or in Killarney
And if he'll go with me, we'll go rovin' through Killkenny
And I'm sure he'll treat me better than my own a-sporting Jenny()