I'm Not Crying
So, you're leaving? I can tell.
Because I can see you leaving...
But if you're trying to break my heart
Your plan is flawed from the start
You can't break my heart, it's liquid
It melted when I met you
And as you leave
Don't turn back to me
Don't turn around and see if I'm crying
I'm not crying (not crying, not crying...)I'm not crying
It's just been raining on my face
And if you think you see some tear tracks down my cheeks
Please (please) don't tell my matesI'm not crying
No, I'm not crying
And if I am crying
It's not cuz of you
It's because I'm thinking of a friend of mine who you don't know who is dying
That's right, dying
These aren't tears of sadness because you're leaving me
I've just been cutting onions
I'm making a lasagna
For one
Oh, I'm not crying
NoThere's just a little bit of dust in my eye
That's from the path that you made when you said your goodbye
I'm not weeping because you won't be here to hold my hand
For your information there's an inflammation in my tear gland
I'm not upset because you left me this way
My eyes are just a little sweaty today
They've been looking around
They're like searching for you
They've been looking for you
Even though I told them not to
These aren't tears of sadness
They're tears of joy
I'm just laughing
Ha ha ha-ha haI'm sitting at this table called love
Staring down at the irony of life
How come we've reached this fork in the road
And yet it cuts like a knife?I'm not cryyyyyyyyying
I'm not cryyyyyyyyying
I'm not cry-y-y-y-y-y-y-y-y-ying