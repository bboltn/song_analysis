Bread Of Heaven
Guide me, O thou great Redeemer,
Pilgrim through this barren land;
I am weak, but thou art mighty;
Hold me with thy powerful hand:
Bread of heaven, bread of heaven
Feed me till I want no more.
Feed me till I want no more.Open thou the crystal fountain
Whence the healing stream shall flow;
Let the fiery, cloudy pillar
Lead me all my journey through:
Strong deliverer, strong deliverer
Be thou still my strength and shield.
Be thou still my strength and shield.When I tread the verge of Jordan,
Bid my anxious fears subside;
Death of death, and hell's destruction,
Land me safe on Canaan's side:
Songs of praises, songs of praises
I will ever give to thee.
I will ever give to thee.