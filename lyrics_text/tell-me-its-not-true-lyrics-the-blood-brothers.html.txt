Tell Me It's Not True
Tell me it's not true,
Say it's just a story,
Something on the news.Tell me it's not true,
Though it's here before me,
Say it's just a dream,
Say it's just a scene,
From an old movie of years ago,
From an old movie of Marilyn Monroe,Say it's just some clowns,
Two players in the limelight,
And bring the curtain down.Say it's just two clowns,
Who couldn't get their lines right,
Say it's just a show on the radio,
That we can turn over and start again,
That we can turn over; it's only a game.Tell me it's not true,
Say I only dreamed it,
And morning will come soon.Tell me it's not true,
Say you didn't mean it,
Say it's just pretend,
Say it's just the end,
Of an old movie from years ago,
From an old movie of Marilyn Monroe.Tell me it's not true,
Say I only dreamed it,
And morning will come soon.Tell me it's not true,say its just a story
Say you didn't mean it.
Say it's just pretend
Say it's just the end
Of an old movie from long ago
From an old movie with Marilyn Monroe.