You'll Never Walk Alone
When you walk through a storm
Hold your head up high
And don't be afraid of the darkAt the end of the storm
Is a golden sky
And the sweet silver song of a larkWalk on through the wind
Walk on through the rain
Though your dreams be tossed and blownWalk on walk on with hope in your heart
And you'll never walk aloneYou'll never walk aloneWalk on walk on with hope in your heart
And you'll never walk aloneYou'll never walk alone