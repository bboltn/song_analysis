(Sitting On) The Dock Of The Bay
Sittin' in the morning sun
I'll be sittin' when the evening comes
Watching the ships roll in
Then I watch them roll away again, yeahI'm sittin' on the dock of the bay
Watchin' the tide roll away, ooh
I'm just sittin' on the dock of the bay
Wastin' timeI left my home in Georgia
Headed for the Frisco Bay
Cuz I've had nothing to live for
And look like nothing's gonna come my waySo, I'm just gon' sit on the dock of the bay
Watchin' the tide roll away, ooh
I'm sittin' on the dock of the bay
Wastin' timeLooks like nothing's gonna change
Everything still remains the same
I can't do what ten people tell me to do
So I guess I'll remain the same, listenSittin' here resting my bones
And this loneliness won't leave me alone, listen
Two thousand miles I roam
Just to make this dock my home, nowI'm just gon' sit at the dock of a bay
Watchin' the tide roll away, ooh
Sittin' on the dock of the bay
Wastin' time