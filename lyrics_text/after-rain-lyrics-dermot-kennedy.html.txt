After Rain
You call arrows to fall short
Because the snow is at our feet
When embraces subside, and the lilies have died
It comes down to her tears on a sheetBut it's alright because
You cause lanterns to light
And force demons disperse
And if Lucifer may fear the swift drying of tears
Then, for evil, you could not be worseBut I see you now, I see you
Release me now, kinda like dreams do
I see you now, was hard to see youJust don't forget to sing
Remember everythingYou're the only little girl I know who'd bring a kite in the snow
Said, "you just gonna hold it up?"
You said, "Nah, I'm gonna let it go"
When your heart hurts
Days like today are the antidote
If you think just maybe it's her
Then promise me, young man, you're gonna let her knowYou won't go lonely, yeah
You won't go lonely, yeah
You won't go lonely, yeah
You won't go lonely, yeah
You won't go lonely, yeahIt'll all be better in the morning
'Cause while you sleep I'll build a wall
But pick a weapon up or something
We're 'bout to have ourselves a brawlIn dead of night, one window open
He heard her singing down the hall
Singing 'bout him, least he was hoping
She left her listener enthralled