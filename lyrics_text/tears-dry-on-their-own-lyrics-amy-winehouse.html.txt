Tears Dry on Their Own
All I can ever be to you
Is a darkness that we know
And this regret I got accustomed toOnce it was so right
When we were at our high
Waiting for you in the hotel at nightI knew I hadn't met my match
But every moment we could snatch
I don't know why I got so attachedIt's my responsibility
And you don't owe nothing to me
But to walk away I have no capacityHe walks away
The sun goes down
He takes the day, but I'm grown
And in your way
In this blue shade
My tears dry on their ownI don't understand
Why do I stress the men
When there's so many bigger things at handWe could have never had it all
We had to hit a wall
So this is inevitable withdrawalEven if I stopped wanting you
A perspective pushes through
I'll be some next man's other woman soonI cannot play myself again?
I should just be my own best friend
Not fuck myself in the head with stupid menHe walks away
The sun goes down
He takes the day, but I'm grown
And in your way
In this blue shade
My tears dry on their ownSo we are history
Your shadow covers me
The sky above ablazeHe walks away
The sun goes down
He takes the day, but I'm grown
And in your way
In this blue shade
My tears dry on their ownI wish I could say no regrets
And no emotional debts
'Cause as we kissed goodbye, the sun setsSo we are history
The shadow covers me
The sky above, a blaze
Only lovers seeHe walks away
The sun goes down
He takes the day, but I'm grown
And in your way
My blue shade
My tears dry on their ownWhoa, he walks away
The sun goes down
He takes the day, but I am grown
And in your way
My deep shade
My tears dry on their ownHe walks away
The sun goes down
He takes the day, but I'm grown
And in your way
My deep shade
My tears dry