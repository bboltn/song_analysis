Big Yellow Taxi
They paved paradise
And put up a parking lot
With a pink hotel, a boutique
and a swinging hot spot
Don't it always seem to go
That you don't know what you've gotTill it's gone
They paved paradise
And put up a parking lot
They took all the trees
And put them in a tree museum
And they charged the people
A dollar and a half just to see 'emDon't it always seem to go
That you don't know what you've got
Till it's gone
They paved paradise
And they put up a parking lot
Hey farmer farmer
Put away that D.D.T. nowGive me spots on my apples
But leave me the birds and the bees
Please!
Dont it always seem to go
That you don't know what you've got
Till it's goneThey paved paradise
And put up a parking lot
Late last night
I heard the screen door slamAnd a big yellow taxi
Took away my old manDon't it always seem to go
That you don't know what you've got
Till it's gone
They paved paradise
And put up a parking lot
I said don't it always seem to go
that you don't know what you've got till its gone
They paved paradise and put up a parking lot
They paved paradise and put up a parking lot
They paved paradise and put up a parking lot