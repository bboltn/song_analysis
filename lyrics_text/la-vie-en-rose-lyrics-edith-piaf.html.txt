La Vie En Rose
Des yeux qui font baisser les miens
Un rire qui se perd sur sa bouche
Voila le portrait sans retouche
De l'homme auquel j'appartiensQuand il me prend dans ses bras
Qu'il me parle tout bas
Je vois la vie en rose.Il me dit des mots d'amour
Des mots de tous les jours
Et ça me fait quelque chose.Il est entré dans mon coeur
Une part de bonheur
Dont je connais la cause.C'est lui pour moi. Moi pour lui dans la vie
Il me l'a dit, l'a juré pour la vie.
Et dès que je l'aperçoisAlors je sens en moi
Mon coeur qui bat
Des nuits d'amour a plus finirUn grand bonheur qui prend sa place
Des enuis des chagrins, s'effacent
Heureux, heureux a en mourir.
Quand il me prend dans ses brasIl me parle tout bas
Je vois la vie en rose.
Il me dit des mots d'amourDes mots de tous les jours
Et ça me fait quelque chose.
Il est entré dans mon coeur
Une part de bonheur
Dont je connais la cause.
C'est toi pour moi. Moi pour toi dans la vie
Il me l'a dit, l'a juré pour la vieuuuh.
Et dès que je t'aperçois
Alors je sens en moi
Mon coeur qui bat