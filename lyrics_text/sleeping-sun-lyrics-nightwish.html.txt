Sleeping Sun
The sun is sleeping quietly
Once upon a century
Wistful oceans calm and red
Ardent caresses laid to restFor my dreams I hold my life
For wishes I behold my night
The truth at the end of time
Losing faith makes a crimeI wish for this night-time
to last for a lifetime
The darkness around me
Shores of a solar sea
Oh how I wish to go down with the sun
Sleeping
Weeping
With youSorrow has a human heart
From my god it will depart
I'd sail before a thousand moons
Never finding where to goTwo hundred twenty-two days of light
Will be desired by a night
A moment for the poet's play
Until there's nothing left to sayI wish for this night-timeto last for a lifetime
The darkness around me
Shores of a solar sea
Oh how I wish to go down with the sun
Sleeping
Weeping
With youI wish for this night-time
to last for a lifetime
The darkness around me
Shores of a solar sea
Oh how I wish to go down with the sun
Sleeping
Weeping
With you