Dark Times
She says she's dead, she doesn't even know why
She's afraid of feeling alive again,
She's trying she's always fooling herself,
Pretends that she has beautiful long hairAt nights she thinks about her bad day,
She cries, she only wants to play videogames,
She's sad, she feels depressed all the timeShe lies, she laughs, that girl is in
Love, she wants to have him but
No, her life is too difficult,
She avoids any chance of being cool,
But why, girl you only have to
Try, you're already beautiful,
You already have a pen to write.Don't shout my name, they already hate me,
My brain is driving me crazy,
He's far, he doesn't live in my city,
It's hard to follow him,Tomorrow I'll be right there,
I'll hide a bible in my bag
To pray he's still alive when I arrive.She lies, she laughs, that girl is in
Love, she wants to have him but
No, her life is too difficult,
She avoids any chance of being cool,
But why, girl you only have to
Try, you're already beautiful,
You already have a pen to write.When will come the moment
When you finally open
Your heart...? Your mind?
When will your innocence be lost?
When will you dare to try?
Then you'll get what you want.She says she's dead, she doesn't even know why
She's afraid of feeling alive again,
She's trying she's always fooling herself,
Pretends that she has beautiful long hairShe lies, she laughs, that girl is in
Love, she wants to have him but
No, her life is too difficult,
She avoids any chance of being cool,
But why, girl you only have to
Try, you're already beautiful,
You already have a pen to write.It's like I can't but I do.