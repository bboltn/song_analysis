Time To Break Free
Where will you be tomorrow?
Really I don't care
I have seen the future, rising everywhere
Look into the mirror, tell me what you see
Piece by piece you'll vanish
Until you disappear!Now I'm doing what I say
No more fooling anyway
Beyond this hell, there is a better life to lead
You're all undone, emotionless
It's not the way for meNow I know there's a better way
Let my heart ride out for a brighter day
Now it's time to breathe in the open air
With a mind so free, anyway
It's time for a change...
It's time to break free!Had enough of fooling, had enough of lies
Hypocrisy, intriguing, right before my eyes
Had enough of hound dogs, barking all the time
I agree with Elvis, you ain't no friend of mineApart from this there is a better life I know
You're all undone, emotionless
It's time for me to goNow I know there's a better way
Let my heart ride out for a brighter day
Now it's time to breathe in the open air
With a mind so free, anyway
It's time for a change...
It's time to break free!Now I know there's a better way
Let my heart fly free towards a better day
Now it's time to breathe, it's the only way,
let my mind ride out, anyway...Now I know, there's a better way
Let my heart ride out for a better day
Now it's time to breathe in the open air
With a mind so free, anyway
It's time to break free!It's time to be me, FREE.