Parting Glass
Of all the money that e'er I had
I spent it in good company
And all the harm I've ever done
Alas, it was to none but meAnd all I've done for want of wit
To memory now I can't recall
So fill to me the parting glass
Good night and joy be to you allChorus:
So fill to me the parting glass
And drink a health whate'er befalls
Then gently rise and softly call
"Good night and joy be to you all"Of all the comrades that e'er I had
They're sorry for my going away
And all the sweethearts that e'er I had
They'd wish me one more day to stayBut since it fell into my lot
That I should rise and you should not
I'll gently rise and softly call
"Good night and joy be to you all"(Chorus)But since it fell into my lot
That I should rise and you should not
I'll gently rise and softly call
"Good night and joy be to you all"(Chorus)Good night and joy be to you all
1