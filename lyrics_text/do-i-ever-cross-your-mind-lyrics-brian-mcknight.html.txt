Do I Ever Cross Your Mind
I can't remember why we fell apart
From something that was so meant to be
Forever was the promise in our hearts
Now more and more I wonder where you areDo I ever cross your mind?
(Anytime)
Do you ever wake up reaching out for me?
Do I ever cross your mind?
(Anytime)
I miss youStill have your picture in a frame
I hear your footsteps down the hall
I swear I hear your voice, driving me insane
How I wish that you would call to sayDo I ever cross your mind?
(Anytime)
Do you ever wake up reaching out for me?
Do I ever cross your mind?
(Anytime)
I miss you, I miss you, I miss youNo more loneliness and heartache
No more crying myself to sleep
No more wondering about tomorrow
Won't you come back to me?
(Come back to me)
Ooh woo oooDo I ever cross your mind?
(Anytime)
Do you ever wake up reaching out for me?
Do I ever cross your mind?
(Anytime)
I miss youDo I ever cross your mind?
(Anytime)
Do you ever wake up reaching out for me?
Do I ever cross your mind?
(Anytime)
I miss you