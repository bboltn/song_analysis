The Pink Panther Theme
Think of all the animals you've ever heard about
Like rhinoceroses and tigers, cats and mink
There are lots of funny animals in all this world
But have you ever seen a panther that is pink?Think!A panther that is positively pink,Well here he is, the pink panther,
The rinky-dink panther,
Isn't he a panther ever so pink?He really is a groovy cat,
And what a gentleman, a scholar, what an acrobat!He's in the pink, the pink panther
The rinky-dink panther,
And it's as plain as your nose,
That he's the one and only, truly original,
Panther-pink (panther) from head to toes !