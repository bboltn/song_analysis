You Make Me Brave
I stand before You now
The greatness of your renown
I have heard the majesty and wonder of you
King of Heaven, in humility, I bowAs Your love, in wave after wave
Crashes over me, crashes over me
For You are for us
You are not against us
Champion of Heaven
You made a way for all to enter inI have heard You calling my name
I have heard the song of love that You sing
So I will let You draw me out beyond the shore
Into Your grace
Into Your graceAs Your love, in wave after wave
Crashes over me, crashes over me
For You are for us
You are not against us
Champion of Heaven
You made a way for all to enter inAs Your love, in wave after wave
Crashes over me, crashes over me
For You are for us
You are not against us
Champion of Heaven
You made a way for all to enter inYou make me brave
You make me brave
You call me out beyond the shore into the waves
You make me brave
You make me brave
No fear can hinder now the love that made a wayYou make me brave
You make me brave
You call me out beyond the shore into the waves
You make me brave
You make me brave
No fear can hinder now the promises You makeYou make me brave
You make me brave
You call me out beyond the shore into the waves
You make me brave
You make me brave
No fear can hinder now the promises You makeYou make me brave
You make me brave
You call me out beyond the shore into the waves
You make me brave
You make me brave
No fear can hinder now the promises You makeAs Your love, in wave after wave
Crashes over me, crashes over me
For You are for us
You are not against us
Champion of Heaven
You made a way for all to enter inAs Your love, in wave after wave
Crashes over me, crashes over me
For You are for us
You are not against us
Champion of Heaven
You made a way
Champion of Heaven
You made a way for all to enter in